import numpy as np
import scipy.fftpack as sf
import scipy.misc
import scipy.signal
import scipy.ndimage as ndimage
import skimage.restoration

import matplotlib.pyplot as plt

from .geometry import Point, Rect, Vec2d
from . import zern

def get_radial_coordinate(width, height, x_center=None, y_center=None):
    if x_center is None:
        x_center = width / 2
    if y_center is None:
        y_center = height / 2
    U, V = np.meshgrid(np.arange(width), np.arange(height))
    R = np.sqrt((U-x_center)**2 + (V-y_center)**2)
    return R

def get_wavefront(fringes, zoi=None, peak_position=None, center_pupil=None, diameter=None, show_plots=True):
    if isinstance(peak_position, tuple):
        peak_position = Vec2d(*peak_position)
    if isinstance(center_pupil, tuple):
        center_pupil = Vec2d(*center_pupil)
    if isinstance(zoi, tuple):
        zoi = Rect(*zoi)

    im = fringes / fringes.ravel().max()

    shape_factor = 2
    
    # Fourier Transform the image
    ft_im = sf.fftshift(sf.fft2(im))

    if show_plots:
        plt.figure()
        plt.imshow(np.log10(abs(ft_im) ** 2))
    if zoi is None:
        if show_plots:
            plt.show()
        raise ValueError('Please provide zone of interest of the Fourier Transform, and peak position.')

    # Cut the interesting part of the fourier transform and paste it with the peak at the center
    h, w = int(shape_factor*im.shape[0]), int(shape_factor*im.shape[1])
    center = Vec2d(w // 2, h // 2)
    ft_reconstructed = np.zeros((h, w), dtype=np.complex128)

    shift_in_zoi = peak_position - Vec2d(zoi.x0, zoi.y0)
    final_zone = zoi.translate(-Vec2d(zoi.x0, zoi.y0) + center - shift_in_zoi)
    
    interesting_part = ft_im[zoi.slice()]
    ft_reconstructed[final_zone.slice()] = interesting_part

    # Apodize the reconstructed fourier transform
    R = get_radial_coordinate(w, h, center.x, center.y)
    ft_reconstructed *= np.exp(-R**2/20**2)

    # Fourier transform back
    complex_im_recovered = sf.ifft2(sf.fftshift(ft_reconstructed))

    # Get the phase 
    phase = np.imag(np.log(complex_im_recovered))

    # phase[phase < 0] = phase[phase < 0] + 2*np.pi

    if show_plots:
        fig, ax = plt.subplots()
        ax.imshow(phase)
        ax.add_artist(plt.Circle((center_pupil.x, center_pupil.y), diameter/2, fill=False))
    if center_pupil is None:
        if show_plots:
            plt.show()
        raise ValueError('Please provide pupil information')
        
    # Crop the phase out of the pupil
    R = get_radial_coordinate(w, h, center_pupil.x, center_pupil.y)
    phase_cropped = phase
    phase_cropped[R > diameter/2] = phase[center_pupil.y, center_pupil.x]
    phase_cropped -= phase_cropped[center_pupil.y, center_pupil.x]

    # Unwrap the phase
    phase_cropped = skimage.restoration.unwrap_phase(phase_cropped)

    return phase_cropped

def fit_zernike(wavefront, endmode=10, startmode=3, center_pupil=(-0.5, -0.5), radius_pupil=-0.5):
    # Fit zernike coefficients
    return zern.fit_zernike(wavefront, nmodes=endmode, startmode=startmode, center=(center_pupil[0], center_pupil[1]), rad=radius_pupil)

