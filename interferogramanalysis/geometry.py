class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Vec2d(Point):
    def __add__(self, other):
        return Vec2d(self.x + other.x, self.y + other.y)
    def __sub__(self, other):
        return Vec2d(self.x - other.x, self.y - other.y)
    def __neg__(self):
        return Vec2d(-self.x, -self.y)

class Rect(object):
    def __init__(self, x0, y0, x1=None, y1=None, w=None, h=None):
        self.x0 = x0
        self.y0 = y0
        if x1 is None:
            self.w = w
            self.h = h
        else:
            self.h = y1 - y0
            self.w = x1 - x0
    
    @property
    def x1(self):
        return self.x0 + self.w
    @property
    def y1(self):
        return self.y0 + self.h

    @x1.setter
    def x1(self, x1):
        self.w = x1 - self.x0
    @y1.setter
    def y1(self, y1):
        self.h = y1 - self.y1

    def slice(self):
        return slice(self.y0, self.y0+self.h), slice(self.x0, self.x0+self.w)

    def translate(self, vector):
        return Rect(self.x0 + vector.x, self.y0 + vector.y, 
                    w=self.w, h=self.h)

    def __repr__(self):
        return '<Rect {self.x0} {self.y0} {self.w} {self.h}>'.format(self=self)

if __name__ == '__main__':
    r = Rect(1, 5, w=3, h=6)
    r = r.translate(Vec2d(0, 0))
    print(r.slice())
