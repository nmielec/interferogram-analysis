import matplotlib.pyplot as plt
import scipy.misc
import numpy as np

from interferogramanalysis.geometry import Rect, Vec2d
from interferogramanalysis.fringe_analysis import get_wavefront, fit_zernike, get_radial_coordinate


image_path = 'exampledata/2017_12_13_18_22_05-image.png'
# original coordinates
pupil = Rect(139, 18, 476, 329)
pupil = Rect(0, 0, 640, 480)
# pupil coordinates
peak = Vec2d(321+1, 280)
zoi = Rect(303, 264, 369, 302)
center_pupil = Vec2d(600, 320)
diameter = 500 #560

fringes = scipy.misc.imread(image_path)

phase = get_wavefront(fringes, zoi, peak, center_pupil, diameter)

zern_coeffs, zern_rec, fit_diff= fit_zernike(phase, nmodes=20, startmode=0,
                                        center_pupil=(center_pupil.x, center_pupil.y), radius_pupil=diameter/2)

# zern_rec /= 2*np.pi
zern_rec -= zern_rec.ravel().min()

plt.figure()
plt.imshow(zern_rec / (2*np.pi), cmap=plt.get_cmap('coolwarm', 9))
plt.title(r'Phase in units of $\lambda$')
plt.colorbar()

R = get_radial_coordinate(zern_rec.shape[1], zern_rec.shape[0])
wf_in_pupil = zern_rec[R < zern_rec.shape[1]]

peakpeak = wf_in_pupil.max() - wf_in_pupil.min()
rms = np.sqrt(1/len(wf_in_pupil.ravel()) * (wf_in_pupil**2).ravel().sum())

print('Peak-peak : {0:.2f} rad | ~lda/{1:.0f}'.format(peakpeak, 2*np.pi / peakpeak))
print('RMS : {0:.3f} rad | ~lda/{1:.0f}'.format(rms, 2*np.pi/rms))


plt.show()